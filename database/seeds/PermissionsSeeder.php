<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit photos']);
        Permission::create(['name' => 'delete photos']);
        Permission::create(['name' => 'publish photos']);
        Permission::create(['name' => 'create permissions']);
        Permission::create(['name' => 'edit permissions']);
        Permission::create(['name' => 'read permissions']);
        Permission::create(['name' => 'delete permissions']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'edit roles']);
        Permission::create(['name' => 'read roles']);
        Permission::create(['name' => 'delete roles']);
        Permission::create(['name' => 'assign roles']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'basic']);

        $role2 = Role::create(['name' => 'admin']);
        $role2->givePermissionTo('edit photos');
        $role2->givePermissionTo('publish photos');

        $role3 = Role::create(['name' => 'super-admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
//        $user = Factory(App\User::class)->create([
//            'name' => 'basic',
//            'email' => 'test@example.com',
//            // factory default password is 'password'
//        ]);
//        $user->assignRole($role1);

//        $user = Factory(App\User::class)->create([
//            'name' => 'admin',
//            'email' => 'admin@example.com',
//            // factory default password is 'password'
//        ]);
//        $user->assignRole($role2);

        $user = Factory(App\User::class)->create([
            'name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('secret'),
        ]);
        $user->assignRole($role3);
    }
}
