<div class="flex flex-col justify-center items-center mt-10">

    @section('title', 'Create Photo')

        <form wire:submit.prevent="save">
            @csrf
            <div>
                <label for="title" class="block text-sm font-medium leading-5 text-gray-700">Title</label>
                <div class="mt-1 relative rounded-md shadow-sm">
                    <input wire:model="title" type="text" id="title" class="form-input block w-full pr-10 @error('title') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror sm:text-sm sm:leading-5" placeholder="title" value="{{ old('title') }}" />
                    @error('title')
                    <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                        <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    @enderror
                </div>
                @error('title')
                <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <div class="mt-2">
                <label for="caption" class="block text-sm font-medium leading-5 text-gray-700">Caption</label>
                <div class="mt-1 relative rounded-md shadow-sm">
                    <div id="caption"
                        wire:ignore
                        x-data
                        @trix-blur="$dispatch('change', $event.target.value)"
                    >
                
                       <trix-editor wire:model.lazy="caption" class="form-textarea block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"></trix-editor> 
                    </div>
                    @error('caption')
                    <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                        <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    @enderror
                </div>
                @error('caption')
                <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <div class="mt-2">
                <label for="newPhoto" class="block text-sm font-medium leading-5 text-gray-700">Photo</label>
                <div class="mt-1 relative rounded-md shadow-sm">
                    <input type="file" wire:model="newPhoto" id="newPhoto" class="form-input block w-full pr-10 @error('newphoto') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror sm:text-sm sm:leading-5" placeholder="new photo" value="{{ old('newPhoto') }}" />
                    @error('newPhoto')
                    <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                        <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    @enderror
                    <div wire:loading wire:target="newPhoto" class="text-gray-500 absolute inset-y-1 right-0 pr-3 flex items-center pointer-events-none">Uploading...</div>
                </div>    
                @error('newPhoto')
                    <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex flex-col mt-2">
                <div class="flex justify-between">
                   <label for="newTags" class="block text-sm font-medium leading-5 text-gray-700">Tags</label>
                   <span class="text-sm leading-5 text-gray-500">Optional</span>
                </div>
                <span class="relative z-0 inline-flex shadow-sm mt-2">
                    <select wire:model="newTags" multiple class="block appearance-none w-full bg-white border hover:border-gray-500 px-4 py-2 pr-8 rounded-md leading-tight focus:outline-none focus:shadow-outline">
                        @foreach ($tags as $tag)
                           <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </span>
            </div>

            <span class="inline-flex rounded-md shadow-sm mt-4">
            <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
            submit
            </button>
        </span>
        </form>
    </div>

    @push('scripts')
        <script src="https://unpkg.com/trix@1.2.3/dist/trix.js"></script>
    @endpush
