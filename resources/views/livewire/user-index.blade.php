<div class="">
    @if (session()->has('success'))
        @include('partials.messages._success')
    @endif
    @section('title', 'Users')
    <div class="md:flex md:items-center md:justify-between">
        <div class="flex-1 min-w-0">
            <div class="flex-shrink-1 ml-0 sm:ml-3 relative w-64 lg:ml-0">
            <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
                <svg class="h-6 w-6 text-gray-600" fill="none" viewbox="0 0 24 24">
                    <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" stroke-linecap="round" stroke-width="2" stroke="currentColor"/>
                </svg>
            </span>
            <input wire:model="search" type="text" placeholder="Search..." type="text" class="focus:border-purple-600 block w-full rounded border border-gray-400 pl-10 pr-4 py-2 text-sm text-gray-900 placeholder-gray-600"/>
        </div>
        </div>
        <div class="mt-4 flex md:mt-0 md:ml-4">
            <span class="ml-2 relative z-0 inline-flex shadow-sm">
              <select wire:model="perPage" class="-ml-px block form-select w-full pl-3 pr-9 py-2 rounded-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                <option value=10>10 per page</option>
                <option value=25>25 per page</option>
                <option value=50>50 per page</option>
              </select>
            </span>
        </div>
    </div>

    <div x-data="{success: true}" class="relative flex flex-col mt-6">

        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                    <tr>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            User ID
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Name
                        </th>
                        {{-- <th class="flex justify-between px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            <a wire:click.prevent="sortBy('email')" role="button" href="#" class="mr-2">Email</a>
                            @include('partials._sort-icon', ['field' => 'email'])
                        </th> --}}
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Email
                        </th>
                        <th class="flex px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            <a wire:click.prevent="sortBy('created_at')" role="button" href="#" class="mr-2">Created</a>
                            @include('partials._sort-icon', ['field' => 'created_at'])
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr class="bg-white">
                            <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                                {{ $user->id }}
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                {{ $user->name }}
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                {{ $user->email }}
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                {{ $user->created_at->diffForHumans() }}
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                <div class="flex justify-between">
                                    <a href="{{ route('user-profile', $user->id) }}" class="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">View</a>
                                    <a href="{{ route('user-profile', $user->id) }}" class="ml-4 text-red-600 hover:text-red-900 focus:outline-none focus:underline">Edit</a>
                                </div>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

{{-- pagination--}}


                <div class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">

                    <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                            <p class="text-sm leading-5 text-gray-700">
                                Showing
                                <span class="font-medium"> {{ $users->firstItem() }} </span>
                                to
                                <span class="font-medium"> {{ $users->lastItem() }} </span>
                                of
                                <span class="font-medium">{{ $users->total() }}</span>
                                results
                            </p>
                        </div>

                        {{ $users->links('partials._pagination') }}

                    </div>
                </div>
                {{--       end paginagtion         --}}
            </div>
        </div>


    </div>
</div>

