

<div class="bg-white overflow-hidden shadow rounded-lg col-span-3 sm:col-span-1">
  <div class="bg-gray-50 border-b border-gray-200 px-4 py-5 sm:px-6">
    <h3 class="text-xl font-bold leading-tight text-gray-900">
        Create Tag
    </h3>
  </div>
  <div class="px-4 py-5 sm:p-6">
    <form wire:submit.prevent="createTag">
        <div>
            <label for="name" class="block text-sm font-medium leading-5 text-gray-700">Name</label>
            <div class="mt-2 relative rounded-md shadow-sm">
                <input wire:model="name" id="name" class="form-input block w-full sm:text-sm sm:leading-5" placeholder="Name" />
            </div>
        </div>

        <div class="flex justify-between mt-6">
            <div class="">
                <label for="color" class="block text-sm font-medium leading-5 text-gray-700">Tag Color</label>
                <span class="relative z-0 inline-flex shadow-sm">
                      <select wire:model="color" class="-ml-px mt-2 block form-select w-full pl-3 pr-9 py-2 rounded-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                          <option value="">Select Color</option>
                          <option value="gray">gray</option>
                          <option value="red">red</option>
                          <option value="orange">orange</option>
                          <option value="yellow">yellow</option>
                          <option value="green">green</option>
                          <option value="teal">teal</option>
                          <option value="blue">blue</option>
                          <option value="indigo">indigo</option>
                          <option value="purple">purple</option>
                          <option value="pink">pink</option>
                      </select>
                </span>
            </div>

            <div class="mr-4">
                <h4 class="font-bold">
                    Preview:
                    <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $color }}-100 text-{{ $color }}-800">
                         {{ $name }}
                    </span>
                </h4>

            </div>
        </div>

        <span class="inline-flex rounded-md shadow-sm mt-4">
              <button type="submit" class="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs leading-4 font-medium rounded text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                  Create
                  <span class="h-4 w-4 ml-2">
                        @include('partials.icons.tag')
                  </span>
              </button>
        </span>
    </form>
  </div>
</div>



{{-- <div class="mt-6 col-span-3 sm:col-span-1">
    <div class="mb-4">
        <h3 class="text-xl font-bold leading-tight text-gray-900">
           Create Tag
        </h3>
    </div>
    <form wire:submit.prevent="createTag">
        <div>
            <label for="name" class="block text-sm font-medium leading-5 text-gray-700">Name</label>
            <div class="mt-2 relative rounded-md shadow-sm">
                <input wire:model="name" id="name" class="form-input block w-full sm:text-sm sm:leading-5" placeholder="Name" />
            </div>
        </div>

        <div class="flex justify-between mt-6">
            <div class="">
                <label for="color" class="block text-sm font-medium leading-5 text-gray-700">Tag Color</label>
                <span class="relative z-0 inline-flex shadow-sm">
                      <select wire:model="color" class="-ml-px mt-2 block form-select w-full pl-3 pr-9 py-2 rounded-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                          <option value="">Select Color</option>
                          <option value="gray">gray</option>
                          <option value="red">red</option>
                          <option value="orange">orange</option>
                          <option value="yellow">yellow</option>
                          <option value="green">green</option>
                          <option value="teal">teal</option>
                          <option value="blue">blue</option>
                          <option value="indigo">indigo</option>
                          <option value="purple">purple</option>
                          <option value="pink">pink</option>
                      </select>
                </span>
            </div>

            <div class="mr-4">
                <h4 class="font-bold">
                    Preview:
                    <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $color }}-100 text-{{ $color }}-800">
                         {{ $name }}
                    </span>
                </h4>

            </div>
        </div>

        <span class="inline-flex rounded-md shadow-sm mt-4">
              <button type="submit" class="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs leading-4 font-medium rounded text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                  Create
                  <span class="h-4 w-4 ml-2">
                        @include('partials.icons.tag')
                  </span>
              </button>
        </span>
    </form>
</div> --}}
