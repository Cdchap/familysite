
<div>
    @section('title', 'Tags')
        <div class="flex justify-between items-center">
            <div class="ml-0 flex-shrink-1 sm:ml-3 relative w-64 lg:ml-0">
                <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
                    <svg class="h-6 w-6 text-gray-600" fill="none" viewbox="0 0 24 24">
                        <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" stroke-linecap="round" stroke-width="2" stroke="currentColor"/>
                    </svg>
                </span>
                <input wire:model="search" type="text" placeholder="Search..." type="text" class="focus:border-purple-600 block w-full rounded border border-gray-400 pl-10 pr-4 py-2 text-sm text-gray-900 placeholder-gray-600"/>
            </div>

            <div class="ml-2 sm:ml-0">
                <span class="relative z-0 inline-flex shadow-sm">
                  <select wire:model="perPage" class="-ml-px block form-select w-full pl-3 pr-9 py-2 rounded-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    <option value=10>10 per page</option>
                    <option value=25>25 per page</option>
                    <option value=50>50 per page</option>
                  </select>
                </span>
            </div>
        </div>


    <div class="grid grid-cols-3 gap-4 mt-6">
        <div x-data="{ success: true }" class="relative flex flex-col col-span-3 sm:col-span-2">
            @if (session()->has('success'))
                @include('partials.messages._success')
            @endif
            <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                    <table class="min-w-full">
                        <thead>
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Name
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Color
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Photos Count
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Art Count
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($tags as $tag)
                            <tr class="bg-white">
                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                                    {{ $tag->name }}
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                    <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $tag->color }}-100 text-{{ $tag->color }}-800">
                                        {{ $tag->color }}
                                    </span>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                    {{ count($tag->photos) > 0 ? count($tag->photos) : 0 }}
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                    Count?
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                    <a href="{{ route('tag-edit', $tag->id) }}" class="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">Edit</a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @livewire('tag-create')
    </div>





</div>
