<div class="">
    @section('title', 'Home')
    
    @if(session('status'))
        <div>
            {{ session('status') }}
        </div>
    @endif
    <div class="flex-shrink-1 ml-3 relative w-64 lg:ml-0 mt-6">
        <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
            <svg class="h-6 w-6 text-gray-600" fill="none" viewbox="0 0 24 24">
                <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" stroke-linecap="round" stroke-width="2" stroke="currentColor"/>
            </svg>
        </span>
        <input wire:model="search" type="text" placeholder="Search..." type="text" class="focus:border-purple-600 block w-full rounded-sm border border-gray-400 pl-10 pr-4 py-2 text-sm text-gray-900 placeholder-gray-600"/>
    </div>
    @foreach($photos as $photo)
        <div class="grid sm:grid-cols-3 grid-cols-1 my-12 border shadow-sm rounded-sm overflow-hidden bg-white">
            <div class="col-span-2">
                <img class="object-cover w-full" src="{{ $photo->getMedia('photos')->first()->getUrl('medium') }}" height="400" width="500" alt="{{ $photo->title }}">
            </div>

            <div class="pt-4">
                <div class="mt-4 sm:mt-0  px-4">
                    @foreach($photo->tags as $tag)
                        <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $tag->color }}-100 text-{{ $tag->color }}-800">
                         {{ $tag->name }}
                        </span>
                    @endforeach
                </div>
                <div class="md:h-full flex-none pt-2 px-4 mt-3 pb-8">
                    <h2 class="font-bold text-xl mb-2">{{ $photo->title }} </h2>
                    <p class="">
                        {!! $photo->caption !!}
                    </p>
                </div>
            </div>
        </div>
    @endforeach

{{--  Pagination  --}}
        <div class="px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">

            <div class="flex flex-col justify-center  w-full items-center sm:flex-1 sm:flex sm:flex-row sm:items-center sm:justify-between">
                <div class="mb-6 sm:m-0">
                    <p class="text-sm leading-5 text-gray-700">
                        Showing
                        <span class="font-medium"> {{ $photos->firstItem() }} </span>
                        to
                        <span class="font-medium"> {{ $photos->lastItem() }} </span>
                        of
                        <span class="font-medium">{{ $photos->total() }}</span>
                        results
                    </p>
                </div>

                {{ $photos->links('partials._pagination') }}

            </div>
        </div>
{{--  end pagination  --}}

</div>
