<div class="flex flex-col justify-center items-center h-screen">
    <div class="">
        @include('partials.icons._coffee')
    </div>
    <h2 class="mt-6">Nothing to see here yet&hellip;coming soon. Head <a href="{{ route('home') }}" class="text-purple-600 hover:underline">home</a></h2>

</div>
