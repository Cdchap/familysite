<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials._head')

<body class="antialiased">
    <main class="max-w-lg min-h-screen flex flex-col justify-center mx-auto">
        <div class="grid grid-cols-2 grid-rows-3 gap-2 ">
                <div class="flex flex-col">
                    <img src="{{ asset('images/nora.jpg') }}" alt="Nora" class="w-full h-auto" style="">
                </div>

                <div class="flex flex-col">
                    <img src="{{ asset('images/christian.jpg') }}" alt="Christian" class="w-full h-auto">
                </div>


                <div class="flex flex-col">
                    <img src="{{ asset('images/jude.jpg') }}" alt="Jude" class="w-full h-auto">
                </div>

                <div class="flex flex-col">
                    <img src="{{ asset('images/abe.jpg') }}" alt="Abraham" class="w-full h-auto">
                </div>
            <div class="row-start-1 row-end-2 col-start-2 col-end-3">
                <div class="flex flex-col justify-center items-center text-gray-400 p-4 h-full">
                    <svg class="" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 66.66 47.14">
                        <title>the-chapman-family</title>
                        <path d="M4.23,15.12H1.53V12h8.6v3.12H7.42v12H4.23Z" transform="translate(-1.53 -12)" />
                        <path d="M19.85,12V27.14H16.64V21H14.4v6.19H11.19V12H14.4v5.89h2.24V12Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M20.93,27.14V12h8.56v3.06H24.12V18h4.69V21H24.12v3h5.37v3.08Z"
                            transform="translate(-1.53 -12)" />
                        <path
                            d="M10.13,37.5V39c0,2.59-1.54,4.46-4.29,4.46S1.55,41.55,1.55,39V32.18c0-2.62,1.56-4.47,4.29-4.47s4.29,1.85,4.29,4.47v1.43H7.07v-1c0-.64-.06-1.87-1.23-1.87S4.61,32,4.61,32.62v5.9c0,.63.06,1.86,1.23,1.86s1.23-1.23,1.23-1.86v-1Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M19.88,28V43.14H16.66V37H14.42v6.19H11.21V28h3.21v5.89h2.24V28Z"
                            transform="translate(-1.53 -12)" />
                        <path
                            d="M26.74,43.14l-.33-2.64H24l-.33,2.64H20.54L22.74,28h4.92l2.2,15.14Zm-2.31-5.46H26l-.77-6.49Z"
                            transform="translate(-1.53 -12)" />
                        <path
                            d="M34.84,28c3.54,0,4.62,2.42,4.62,4.49,0,3-1.5,5.08-4.49,5.08H34v5.57H30.9V28ZM34,34.42h.74c1.74,0,1.68-1.23,1.68-1.87A1.4,1.4,0,0,0,34.84,31H34Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M48.83,28V43.14H45.77V34.75l-1.21,4.56-1.21-4.56v8.39H40.27V28h3.08l1.21,4.6L45.77,28Z"
                            transform="translate(-1.53 -12)" />
                        <path
                            d="M55.78,43.14l-.33-2.64H53l-.33,2.64H49.57L51.77,28H56.7l2.2,15.14Zm-2.31-5.46H55l-.77-6.49Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M65.13,43.14l-2.42-7.62v7.62H59.63V28h3.08l2.42,7.74V28h3.06V43.14Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M5.11,53v6.14H1.9V44h8.56v3.06H5.11V50H9.65v3Z" transform="translate(-1.53 -12)" />
                        <path
                            d="M17.06,59.14l-.33-2.64H14.31L14,59.14H10.86L13.06,44H18l2.21,15.14Zm-2.31-5.46h1.54l-.77-6.49Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M29.47,44V59.14H26.41V50.75L25.2,55.31,24,50.75v8.39H20.91V44H24l1.21,4.6L26.41,44Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M39.17,56.14v3H30.61v-3h2.71V47H30.7V44h8.43v3H36.51v9.15Z"
                            transform="translate(-1.53 -12)" />
                        <path d="M40.27,44h3.19V56h5.37v3.19H40.27Z" transform="translate(-1.53 -12)" />
                        <path d="M58.73,44l-2.86,9.15v6H52.66v-6L49.75,44h3.17l1.32,4.79L55.56,44Z"
                            transform="translate(-1.53 -12)" />
                    </svg>
                </div>
            </div>
            <div class="col-start-1 col-end-2 row-start-3 row-end-4 flex flex-col justify-center items-center">
                <div class="flex justify-between ">
                    @auth
                        <a class="font-bold text-gray-400 hover:text-gray-500"
                            href="{{ route('home') }}">Home</a>
                    @else
                        <a class="font-bold text-gray-400 hover:text-gray-500"
                            href="{{ route('login') }}">Sign
                            in</a>

                        <a class="font-bold text-gray-400 ml-8 hover:text-gray-500"
                            href="{{ route('register') }}">Register</a>

                    @endauth
                </div>
            </div>

        </div>
    </main>
</body>

</html>
