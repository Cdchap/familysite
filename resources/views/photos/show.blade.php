@extends('layouts.admin')

@section('title', 'Viewing: ' . $photo->title)

@section('content')
    <div class="flex flex-col justify-center items-center">


        <div class="bg-white shadow sm:rounded-lg">
            <div class="px-4 py-5 sm:p-6">
                <div>
                    <img src="{{ $photo->getMedia('photos')->first()->getUrl('medium') }}" alt="{{ $photo->title }}">
                </div>
                <h4 class="mt-2 text-sm italic text-gray-500">
                    created: {{ $photo->created_at->diffForHumans() }}
                </h4>
                <h3 class="text-lg leading-6 font-medium text-gray-900 mt-4">
                    {{ $photo->title }}
                </h3>
                <div class="mt-2 max-w-xl text-sm leading-5 text-gray-500">
                    <p>
                        {!! $photo->caption !!}
                    </p>
                </div>
                <div class="mt-5">
                    <a href="{{ route('photo-edit', $photo->id) }}" class="inline-flex items-center justify-center px-4 py-2 border border-transparent font-medium rounded-md text-green-700 bg-green-100 hover:bg-green-50 focus:outline-none focus:border-green-300 focus:shadow-outline-green active:bg-green-200 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                        Edit Photo
                    </a>
                </div>
            </div>
        </div>


    </div>
@endsection
