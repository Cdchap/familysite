<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('partials._head')

<body class="font-sans antialiased">

        <main class="">
            @yield('content')
        </main>

</body>
</html>
