<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials._head')

<body class="font-sans antialiased">

<div>
    @include('partials._admin-nav')

    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="text-3xl font-bold leading-tight text-gray-900">
                @yield('title')
            </h2>
        </div>
    </header>
    <main>
        <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
            <!-- Replace with your content -->
            <div class="px-4 py-6 sm:px-0">
                <div class="">
                  @yield('content')

                </div>
            </div>
            <!-- /End replace -->
        </div>
    </main>
</div>
@livewireScripts
@stack('scripts')
</body>
</html>
