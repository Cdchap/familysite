<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('partials._head')

<body class="font-sans antialiased relative bg-gray-50">

    @include('partials._nav')


        <main  class="font-sans container mx-auto px-0 md:px-4 md:px-0 max-w-4xl flex flex-col mb-8">

            @yield('content')

        </main>
    @include('partials._footer')
    @livewireScripts
</body>
</html>
