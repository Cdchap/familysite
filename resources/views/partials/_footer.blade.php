<div class="font-sans font-bold text-sm text-gray-400 bg-gray-50 h-48 flex flex-col justify-center px-4">
    Copyright &copy; {{ date('Y') }} chappeople.com
</div>
<div class="text-teal-200 h-20 flex flex-col justify-center items-center">
    @include('partials.site-logo')
</div>
