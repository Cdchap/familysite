<div class="sticky z-50 h-16 bg-white bg-white border border-b border-gray-800 top-0 flex flex-col justify-center">
    <nav x-data="{open: false}" @keydown.window.escape="open = false" class="">
        <div class="container mx-auto px-4 xl:px-0 max-w-6xl flex justify-between items-center md:justify-start">
            <div class="flex flex-col justify-center font-bold text-sm text-gray-800 tracking-wide px-3">
                <div>
                    @include('partials.site-logo')
                </div>
            </div>
            <div class="hidden md:block flex ml-6">
                <a href="{{ route('home') }}"class="{{ request()->url() === route('home') ? 'border border-gray-700 bg-teal-200' : '' }} ml-2 px-2 py-2 text-sm font-medium text-gray-700 hover:bg-gray-200 focus:outline-none focus:bg-gray-200">Photos</a>
                <a href="{{ route('art') }}"class="{{ request()->url() === route('art') ? 'border border-gray-700 bg-teal-200' : '' }} ml-4 px-2 py-2 text-sm font-medium text-gray-700 hover:bg-gray-200 focus:outline-none focus:bg-gray-200">Artwork</a>
                <a href="{{ route('video') }}"class="{{ request()->url() === route('video') ? 'border border-gray-700 bg-teal-200' : '' }} ml-4 px-2 py-2 text-sm font-medium text-gray-700 hover:bg-gray-200 focus:outline-none focus:bg-gray-200">Video</a>
                @can('edit photos')
                    <a href="{{ route('admin') }}"class="ml-4 px-2 py-2 text-sm font-medium text-gray-700 hover:bg-gray-200 focus:outline-none focus:bg-gray-200">Admin</a>
                @endcan
            </div>

            <div class="">
                <div class="ml-4 flex items-center md:ml-6">
                    <div @click.away="open = false" class="ml-3 relative" x-data="{ open: false }">
                        <div>
                            <button @click="open = !open" class="max-w-xs flex items-center text-sm border border-2 border-gray-700 text-gray-700 hover:bg-teal-200 pl-3 pr-1 py-1 focus:outline-none focus:shadow-solid">
                                Menu
                                <svg class="h-6 w-6" stroke="currentColor" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path class="heroicon-ui" d="M15.3 9.3a1 1 0 0 1 1.4 1.4l-4 4a1 1 0 0 1-1.4 0l-4-4a1 1 0 0 1 1.4-1.4l3.3 3.29 3.3-3.3z"/>
                                </svg>
                            </button>
                        </div>
                        <div x-show="open" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg">
                            <div class="py-1 bg-white shadow-xs">
                                <a href="{{ route('home') }}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Photos</a>
                                <a href="{{ route('art') }}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Art</a>
                                <a href="{{ route('video') }}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 ">Videos</a>
                                @can('edit photos')
                                    <a href="{{ route('admin') }}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Admin</a>
                                @endcan
                                <a class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Sign out') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>



