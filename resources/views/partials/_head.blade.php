<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    {{--  alpineJS and turbolinks  --}}
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.0/dist/alpine.js" defer></script>
    <script src="{{ asset('js/app.js') }}"></script>

    {{--  fonts  --}}
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

    {{--  styles  --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">

    @livewireStyles
</head>
