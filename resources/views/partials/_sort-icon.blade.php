@if ($sortField !== $field)
    @include('partials.icons.switch-verticle')
@elseif ($sortAsc)
    @include('partials.icons.sort-asc')
@else
    @include('partials.icons.sort-desc')
@endif
