@extends('layouts.auth')

@section('content')
    <div class="container mx-auto flex flex-col justify-center items-center h-screen">
        <h2 class="text-4xl sm:text-5xl text-gray-500 uppercase font-bold">Reset Password</h2>
        <div class="w-full max-w-xs">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('password.email') }}" class="border border-2 px-8 pt-6 pb-8 mb-4">
                @csrf
                <div class="mb-4">
                    <label for="email" class="block text-sm text-gray-700 font-bold mb-2">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="border appearance-none w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline
                       @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}"
                           required
                           autocomplete="email"
                           autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="flex items-center justify-between">
                    <button type="submit" class="bg-purple-700 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                        {{ __('Send Reset Password Link') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
