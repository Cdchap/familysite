@extends('layouts.auth')

@section('content')

    <div class="flex flex-col justify-center items-center h-screen">
        <div class="">
            @include('partials.icons._whatever')
        </div>
        <h2 class="mt-6 text-xl"><strong>404</strong> page does not exist. Head <a href="{{ route('home') }}"
                class="text-purple-600 hover:underline">home</a></h2>
    </div>

@endsection