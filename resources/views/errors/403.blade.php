@extends('layouts.auth')

@section('content')

    <div class="flex flex-col justify-center items-center h-screen">
        <div class="">
            @include('partials.icons._coffee')
        </div>
        <h2 class="mt-6">{{ $exception->getMessage() }}. <a href="{{ route('login') }}"
                class="text-purple-600 hover:underline">log in</a></h2>
    </div>

@endsection