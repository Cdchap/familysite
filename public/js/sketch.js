let img;
let judeImg;
let noraImg;
let abeImg;
let vid;
let theta = 0;
let beta = 0;
let alpha = 0;
let zeta = 0;

let color
const colors = [
"antiquewhite",
"aqua",
"aquamarine",
"azure",
"beige",
"bisque",
"black",
"blanchedalmond",
"blue",
"blueviolet",
"brown",
"burlywood",
"cadetblue",
"chartreuse",
"chocolate",
"coral",
"cornflowerblue",
"cornsilk",
"crimson",
"cyan",
"darkblue",
"darkcyan",
"darkgoldenrod",
"darkgray",
"darkgreen",
"darkkhaki",
"darkmagenta",
"darkolivegreen",
"darkorange",
"darkorchid",
"darkred",
"darksalmon",
"darkseagreen",
"darkslateblue",
"darkslategray",
"darkturquoise",
"darkviolet",
"deeppink",
"deepskyblue",
"dimgray",
"dodgerblue",
"firebrick",
"floralwhite",
"forestgreen",
"fuchsia",
"gainsboro",
"ghostwhite",
"gold",
"goldenrod",
"gray",
"green",
"greenyellow",
"honeydew",
"hotpink",
"indianred",
"indigo",
"ivory",
"khaki",
"lavender",
"lavenderblush",
"lawngreen",
"lemonchiffon",
"lightblue",
"lightcoral",
"lightcyan",
"lightgoldenrodyellow",
"lightgreen",
"lightgrey",
"lightpink",
"lightsalmon",
"lightseagreen",
"lightskyblue",
"lightslategray",
"lightsteelblue",
"lightyellow",
"lime",
"limegreen",
"linen",
"magenta",
"maroon",
"mediumaquamarine",
"mediumblue",
"mediumorchid",
"mediumpurple",
"mediumseagreen",
"mediumslateblue",
"mediumspringgreen",
"mediumturquoise",
"mediumvioletred",
"midnightblue",
"mintcream",
"mistyrose",
"moccasin",
"navajowhite",
"navy",
"navyblue",
"oldlace",
"olive",
"olivedrab",
"orange",
"orangered",
"orchid",
"palegoldenrod",
"palegreen",
"paleturquoise",
"palevioletred",
"papayawhip",
"peachpuff",
"peru",
"pink",
"plum",
"powderblue",
"purple",
"red",
"rosybrown",
"royalblue",
"saddlebrown",
"salmon",
"sandybrown",
"seagreen",
"seashell",
"sienna",
"silver",
"skyblue",
"slateblue",
"slategray",
"snow",
"springgreen",
"steelblue",
"tan",
"teal",
"thistle",
"tomato",
"turquoise",
"violet",
"wheat",
"white",
"whitesmoke",
"yellow"
]

const SNOWFLAKES_PER_LAYER = 100;
const MAX_SIZE = 10;
const GRAVITY = 0.75;
const LAYER_COUNT = 5;

const WIND_SPEED = 1;
const WIND_CHANGE = 0.0025;

const SNOWFLAKES = [];

const photos = [];

function preload() {
	for(let i = 0; i < 19; i++) {
		let img = loadImage('/images/photo' + i + '.JPG')
		photos.push(img)
	} 

}
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function setup() {
	
	createCanvas(windowWidth, windowHeight, WEBGL);
	noStroke()

	color = random(colors)

	input = createInput()
	input.center()

	greeting = createElement('h1', 'What holiday do you celebrate?')
	greeting.position(input.x, input.y - 65)
	greeting.style('color', 'white')

	button = createButton('submit');
  	button.position(input.x + input.width, input.y);
	button.mousePressed(greet)

	// Initialize the snowflakes with random positions
	for (let l = 0; l < LAYER_COUNT; l++) {
	SNOWFLAKES.push([]);
	for (let i = 0; i < SNOWFLAKES_PER_LAYER; i++) {
	SNOWFLAKES[l].push({
		x: random(width),
		y: random(height),
		mass: random(0.75, 1.25),
		l: l + 1
	});
	}
	}
}

function draw() {
  background(color);
  // Iterate through each snowflake to draw and update them
  translate(- (windowWidth/2), -(windowHeight/2),0)
  for (let l = 0; l < SNOWFLAKES.length; l++) {
    const LAYER = SNOWFLAKES[l];

    for (let i = 0; i < LAYER.length; i++) {
      const snowflake = LAYER[i];
      circle(snowflake.x, snowflake.y, (snowflake.l * MAX_SIZE) / LAYER_COUNT);
      updateSnowflake(snowflake);
    }
  }
// cube
  translate(width/4,100, -200);
  push();
  rotateZ(theta * 0.1);
  rotateX(theta * 0.1);
  rotateY(theta * 0.1);
  texture(photos[0]);
  box(200, 200, 200);
  pop();
  theta += 0.05;

//    cube2
  translate(width/2, 50, 200);
  push();
  rotateZ(alpha * 0.1);
  rotateX(alpha * 0.1);
  rotateY(alpha * 0.1);
  texture(photos[1]);
  box(200, 200, 200);
  pop();
  alpha += 0.03;

// //      cube3
translate(-width/2, 500, -100)
  push();
  rotateZ(zeta * 0.1);
  rotateX(zeta * 0.1);
  rotateY(zeta * 0.1);
  texture(photos[2]);
  box(400, 400, 400);
  pop();
  zeta += 0.02;
}

// Helper function to prepare a given snowflake for the next frame
function updateSnowflake(snowflake) {
  const diameter = (snowflake.l * MAX_SIZE) / LAYER_COUNT;
  if (snowflake.y > height + diameter) snowflake.y = -diameter;
  else snowflake.y += GRAVITY * snowflake.l * snowflake.mass;

  // Get the wind speed at the given layer and area of the page
  const wind =
    noise(snowflake.l, snowflake.y * WIND_CHANGE, frameCount * WIND_CHANGE) -
    0.5;
  if (snowflake.x > width + diameter) snowflake.x = -diameter;
  else if (snowflake.x < -diameter) snowflake.x = width + diameter;
  else snowflake.x += wind * WIND_SPEED * snowflake.l;
}

function mouseClicked() {
	color = random(colors)
	photos = shuffle(photos, true)
}

function greet() {
  const holiday = input.value();
  greeting.html('Happy ' + holiday + '! From the Chapmans');
  greeting.center()
  input.hide()
  button.hide()
}
