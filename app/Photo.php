<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;

class Photo extends Model implements HasMedia
{
    protected $guarded = [];

    use HasMediaTrait;

    public function registerMediaConversions(Media $media = null)
    {

        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(75)
            ->sharpen(10);

        $this->addMediaConversion('medium')
            ->width(1200)
            ->height(900)
            ->sharpen(10);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
