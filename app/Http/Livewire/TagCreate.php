<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Tag;

class TagCreate extends Component
{
    public $name = '';
    public $color ='';

    public function createTag()
    {
        $validatedItems = $this->validate([
            'name' => 'required|min:3|max:16',
            'color' => 'required'
        ]);

        if(!$validatedItems){
            return redirect()
                    ->route('tag-create')
                    ->withInput()
                    ->withErrors();
        } else {
            $validatedItems['name'] = strtoupper($validatedItems['name']);
            Tag::create($validatedItems);
        }


        session()->flash('success', 'Tag has been created');

        return redirect()->route('tag-index');


    }

    public function render()
    {
        return view('livewire.tag-create');
    }

}
