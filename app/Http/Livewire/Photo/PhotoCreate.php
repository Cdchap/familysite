<?php

namespace App\Http\Livewire\Photo;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Photo;
use App\Tag;

class PhotoCreate extends Component
{
    use WithFileUploads;

    public $title = '';
    public $caption = '';
    public $user;
    public $newPhoto;
    public $newTags = [];

    public function mount()
    {
        $this->user = auth()->user();
    }

    public function updatedNewPhoto()
    {
        $this->validate(['newPhoto' => 'image|max:3072']);
    }

    public function save()
    {
        $this->validate([
            'title' => 'required|min:6',
            'caption' => 'required', 
            'newPhoto' => 'required',
            'newTags' => 'array|nullable'
        ]);

        $photo = Photo::create([
            'user_id' => $this->user->id,
            'title' => Str::title($this->title),
            'caption' => $this->caption,
        ]);

        $photo->addMedia($this->newPhoto->path())->toMediaCollection('photos');

        $photo->tags()->sync($this->newTags);

        session()->flash('success', 'The photo "' . $photo->title . '" has been created.');
        return redirect()->route('photo-index');
    }

    public function render()
    {
        return view('livewire.photo.photo-create',
            ['tags' => Tag::all()] 
        );
    }
}
