<?php

namespace App\Http\Livewire\Photo;

use Livewire\Component;
use App\Photo;

class PhotoDelete extends Component
{
    public $photo;
    public $title;

    public function mount(Photo $photo)
    {
        $this->photo = Photo::findOrFail($photo->id);
        $this->title = $photo->title;
    }

    public function delete()
    {

        $this->photo->delete();

        return redirect()->route('photo-index');
    }

    public function render()
    {
        return view('livewire.photo.photo-delete');
    }
}
