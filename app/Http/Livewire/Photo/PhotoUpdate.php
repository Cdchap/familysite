<?php

namespace App\Http\Livewire\Photo;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Photo;
use App\Tag;

class PhotoUpdate extends Component
{
    use WithFileUploads;

    public $title = '';
    public $caption = '';
    public $user;
    public $photo;
    public $mediaItems;
    public $newPhoto;
    public $newTags = [];
    
    public function mount($photo)
    {
        $this->user = auth()->user();

        $this->photo = Photo::findOrFail($photo);
        $this->title = $this->photo->title;
        $this->caption = $this->photo->caption;
        $this->mediaItems = $this->photo->getMedia('photos');

    }

    public function updatedNewPhoto()
    {
        $this->validate(['newPhoto' => 'image|max:3072']);
    }

    public function save()
    {
        $this->validate([
            'title' => 'required|min:6',
            'caption' => 'required', 
            'newTags' => 'array|nullable'
        ]);

        $this->photo->update([
            'user_id' => $this->user->id,
            'title' => Str::title($this->title),
            'caption' => $this->caption,
        ]);

        $this->photo->tags()->sync($this->newTags);

        if($this->newPhoto) {
            $this->mediaItems[0]->delete();
            $this->photo->addMedia($this->newPhoto->path())->toMediaCollection('photos');
        }

        $this->newPhoto = null;

        session()->flash('success', 'The photo "' . $this->photo->title . '" has been created.');
        return redirect()->route('photo-index');
    }

    public function render()
    {
        return view('livewire.photo.photo-update', [
            'tags' => Tag::all()
            ]);
    }
}
