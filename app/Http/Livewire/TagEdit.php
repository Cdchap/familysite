<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Tag;

class TagEdit extends Component
{
    public $tag;
    public $color;
    public $name;

    public function mount(Tag $tag)
    {
        $this->tag = $tag;
        $this->color = $tag->color;
        $this->name = $tag->name;
    }

    public function editTag()
    {
        $validatedItems = $this->validate([
            'name' => 'required|min:3|max:16',
            'color' => 'required'
        ]);

        if (! $validatedItems){
            return redirect()
                    ->route('tag-edit')
                    ->withInput()
                    ->withErrors();
        } else {
            $editTag = Tag::findOrFail($this->tag->id);

            $editTag->name = $this->name;
            $editTag->color = $this->color;

            $editTag->save();

            session()->flash('success', 'Tag has been edited');

            return redirect()->route('tag-index');
        }


    }

    public function render()
    {
        return view('livewire.tag-edit');
    }

}
