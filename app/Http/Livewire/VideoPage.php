<?php

namespace App\Http\Livewire;

use Livewire\Component;

class VideoPage extends Component
{
    public function render()
    {
        return view('livewire.video-page');
    }
}
