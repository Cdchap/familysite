<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use \App\User;

class UserIndex extends Component
{
    use WithPagination;

    protected $updateQuerySting =['search' => ['except' => '']];

    public $search = '';
    public $perPage = 10;
    public $sortField = 'created_at';
    public $sortAsc = true;

     public function sortBy($field)
    {
        if($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        }
        else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        return view('livewire.user-index',
            ['users' => User::search('name', $this->search)
                        ->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc')
                        ->paginate($this->perPage), 
        ]);
    }
}
