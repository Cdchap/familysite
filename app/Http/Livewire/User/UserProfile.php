<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use Spatie\Permission\Models\Role;
use App\User;

class UserProfile extends Component
{
    public $user;
    public $name = '';
    public $email = '';

    public $newRole;

    public function mount(User $user)
    {
        $this->user = User::findOrFail($user->id);
        $this->name = $this->user->name;
        $this->email = $this->user->email;
    }

    public function save()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required|email',
            'newRole' => 'required'
        ]);

        $this->user->update([
            'name' => $this->name,
            'email' => $this->email
        ]);

        $role = Role::findOrFail($this->newRole);
        $this->user->syncRoles($role);

        session()->flash('notify-saved');


    }

    public function delete()
    {
        $this->user->delete();

        session()->flash('success', 'User is deleted');
        return redirect()->route('user-index');
    }

    public function render()
    {
        return view('livewire.user.user-profile', 
            ['roles' => Role::all(),]
        );
    }
}
