<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\User;

class UserDelete extends Component
{
    public $user;
    public $name;
    public function mount(User $user)
    {
        $this->user = User::findOrFail($user->id);
        $this->name = $user->name;
    }

    public function delete() 
    {
        $this->user->delete();

        return redirect()->route('user-index');
    }

    public function render()
    {
        return view('livewire.user.user-delete');
    }
}
