<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Tag;

class TagIndex extends Component
{
    public $search = '';

    public $perPage = 10;

    public function render()
    {
        return view('livewire.tag-index', [
            'tags' => Tag::search(['name'], $this->search)->paginate($this->perPage)
        ]);

    }
}
