<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ArtPage extends Component
{
    public function render()
    {
        return view('livewire.art-page');
    }
}
