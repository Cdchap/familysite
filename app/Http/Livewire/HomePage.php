<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Photo;

class HomePage extends Component
{
    use WithPagination;

    public $search = '';
    public $sortBy ='created_at';

    public function render()
    {
        return view('livewire.home-page',
            ['photos' => Photo::search(['title','tags.name'], $this->search)
                        ->orderBy($this->sortBy, 'desc')
                        ->paginate(10)
        ]);
    }
}
