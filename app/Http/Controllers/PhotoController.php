<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Photo;
use App\Tag;

class PhotoController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('photos.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $validatedItems = $request->validate([
            'title' => 'required|min:6',
            'caption' => 'required',
        ]);

        $validatedTags = $request->validate([
           'tags' => 'array|nullable',
        ]);

        if (!$validatedItems) {
            return redirect('photo-create')
                    ->withErrors()
                    ->withInput();
        }
        else {
            $photo = Photo::create([
                'user_id' => $user->id,
                'title' => Str::title($validatedItems['title']),
                'caption' => $validatedItems['caption'],
            ]);
        }

        if($request->hasFile('photo')){
            $photo->addMediaFromRequest('photo')->toMediaCollection('photos');
        }
        else{
            return redirect()->back()->withErrors();
        }

        if(Arr::has($validatedTags, 'tags')) 
        {
            $tags = $validatedTags['tags'];
            $photo->tags()->sync($tags);
        }

        session()->flash('success', 'The photo "' . $photo->title . '" has been created.');
        return redirect()->route('photo-index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        $photo = Photo::findOrFail($photo->id);

        return view('photos.show', compact('photo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        $photo = Photo::findOrFail($photo->id);

        return view('photos.edit', compact('photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedItems = $request->validate([
            'title' => 'required|min:6',
            'caption' => 'required',
        ]);

        if (!$validatedItems) {
            return redirect('photo-update')
                ->withErrors()
                ->withInput();
        }
        else {
            $photo = Photo::findOrFail($id);

            if ($validatedItems['title'] === $photo->title && $validatedItems['caption'] === $photo->caption && $request->hasFile('photo')){
                $photo->delete();
                $photo->addMediaFromRequest('photo')->toMediaCollection('photos');

            }
            if ($validatedItems['title'] !== $photo->title) {
                $photo->title = $validatedItems['title'];
            }
            if ($validatedItems['caption'] !== $photo->caption) {
                $photo->caption = $validatedItems['caption'];
            }

            $photo->save();

            session()->flash('success', 'The photo has been updated.');

            return redirect()->route('photo-index');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);


        $photo->tags()->detach();

        $photo->delete();

        session()->flash('success', 'Photo deleted.');

        return redirect()->route('photo-index');


    }
}
