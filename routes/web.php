<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');
Route::view('/winter', 'winter.index')->name('winter');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::livewire('/home', 'home-page')->layout('layouts.app')->name('home');
    Route::livewire('/art', 'art-page')->layout('layouts.app')->name('art');
    Route::livewire('/video', 'video-page')->layout('layouts.app')->name('video');
});



Route::group(['middleware' => ['role:super-admin|admin']], function () {
    Route::livewire('/admin', 'admin-dashboard')->layout('layouts.admin')->name('admin');

    Route::livewire('/admin/photos', 'photo-index')->layout('layouts.admin')->name('photo-index');
    Route::livewire('/admin/photos/create', 'photo.photo-create')->layout('layouts.admin')->name('photo-create');
    Route::delete('/admin/photos/{photo}', 'PhotoController@destroy')->name('photo-delete');
    Route::get('/admin/photos/{photo}', 'PhotoController@show')->name('photo-show');
    Route::livewire('/admin/photos/{photo}/edit', 'photo.photo-update')->layout('layouts.admin')->name('photo-edit');
    Route::post('/admin/photos', 'PhotoController@store')->name('photos-store');

    Route::livewire('admin/users', 'user-index')->layout('layouts.admin')->name('user-index');
    Route::livewire('admin/users/profile/{user}', 'user.user-profile')->layout('layouts.admin')->name('user-profile');

    Route::livewire('admin/tags', 'tag-index')->layout('layouts.admin')->name('tag-index');
    Route::livewire('admin/tags/{tag}/edit', 'tag-edit')->layout('layouts.admin')->name('tag-edit');
    // need to finish the CRUD for tags
});


